# File Organization Guidelines

The IICS Swagger File Public Repository is a shared environment, and like any shared environment the users should follow organizational schema.

No Swagger files should exist in the root directory, and should be placed into folders within that directory whenever possible. These folders should be organized based on whatever project you are using these swagger files for, or alternatively by the api the files are created to interact with. If your team is planning on using this repository for many projects, they can create a folder for their team and place project folders within that folder. A text-based illustration of what this schema might potentially look like is located below.

![Text-based illustration of the file organization](file-organization-guidelines-screenshot.png)

The Integration Team will provide several swagger files for integrators to use to interact with several common UW-Madison APIs. These will be provided in a yet-to-be determined folder on this repository.